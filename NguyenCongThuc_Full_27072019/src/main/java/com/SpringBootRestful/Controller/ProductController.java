package com.SpringBootRestful.Controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.SpringBootRestful.Model.ProductModel;
import com.SpringBootRestful.Service.ProductService;

@Controller
public class ProductController {
	@Autowired
	private ProductModel pr;
	
	@GetMapping("/product")
	public String index(Model model) {
		model.addAttribute("product", new ProductModel());
		return "form";
	}
	@GetMapping("/product/create")
	public String create(Model model) {
		model.addAttribute("product", new ProductModel());
		return "form";
	}

	@GetMapping("/product/{code}/edit")
	public String edit(@PathVariable int id, Model model) {
		model.addAttribute("product", ProductService.findOne(id));
		return "form";
	}

	@PostMapping("/product/save")
	public String save(@Valid ProductModel pro, BindingResult result, RedirectAttributes redirect) {
		if (result.hasErrors()) {
			return "form";
		}
		ProductService.save(pro);
		redirect.addFlashAttribute("success", "Saved employee successfully!");
		return "redirect:/product";
	}

	@GetMapping("/product/{id}/delete")
	public String delete(@PathVariable int id, RedirectAttributes redirect) {
		ProductService.delete(id);
		redirect.addFlashAttribute("success", "Deleted employee successfully!");
		return "redirect:/product";
	}

	@GetMapping("/product/search")
	public String search(@RequestParam("s") String s, Model model) {
		if (s.equals("")) {
			return "redirect:/product";
		}

		model.addAttribute("product", ProductService.search(s));
		return "list";
	}
}
