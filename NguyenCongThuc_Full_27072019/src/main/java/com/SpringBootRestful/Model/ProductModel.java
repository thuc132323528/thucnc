package com.SpringBootRestful.Model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SanPham")
public class ProductModel implements Serializable {

	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="code",nullable=false)
	private String code;
	
	@Column(name="name",nullable=false)
	private String name;
	
	@Column(name="price",nullable=false)
	private float price;
	
	@Column(name="des",nullable=false)
	private String des;

	public ProductModel() {
		super();
	}

	public ProductModel(String code, String name, float price, String des) {
		super();
		this.code = code;
		this.name = name;
		this.price = price;
		this.des = des;
	}



	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public float getPrice() {
		return price;
	}

	public void setPrice(float price) {
		this.price = price;
	}

	public String getDes() {
		return des;
	}

	public void setDes(String des) {
		this.des = des;
	}
	
	
}
