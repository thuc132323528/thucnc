package com.SpringBootRestful.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.SpringBootRestful.Model.ProductModel;
import com.SpringBootRestful.Repository.ProductRepository;

@Service	
public class ProductServiceImpl {
	@Autowired
	private ProductRepository pr;
	
	public Iterable<ProductModel> findAll() {
        return pr.findAll();
    }

    public List<ProductModel> search(String q) {
        return pr.findByNameContaining(q);
    }

    public ProductModel findOne(int id) {
        return pr.findOne(id);
    }

    public void save(ProductModel contact) {
    	pr.save(contact);
    }

    public void delete(int id) {
    	pr.delete(id);
    }
}
