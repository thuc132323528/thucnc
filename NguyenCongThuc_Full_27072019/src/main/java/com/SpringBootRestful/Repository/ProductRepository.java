package com.SpringBootRestful.Repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.SpringBootRestful.Model.ProductModel;


public interface ProductRepository extends CrudRepository<ProductModel, Integer> {
	 List<ProductModel> findByNameContaining(String q);
}
