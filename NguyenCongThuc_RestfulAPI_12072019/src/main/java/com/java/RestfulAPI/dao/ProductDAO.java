package com.java.RestfulAPI.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.java.RestfulAPI.model.Product;

public class ProductDAO {

	static ConnectDB db = new ConnectDB();

	public static List<Product> getproducts() {
		List<Product> list = null;
		try {
			Statement st = db.getConnection().createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM HangHoa");
			list = new ArrayList<Product>();
			while (rs.next()) {
				String code = rs.getString(1);
				String name = rs.getString(2);
				float price = rs.getFloat(3);
				String des = rs.getString(4);
				list.add(new Product(code, name, price, des));
			}
			db.getConnection().close();
			st.close();
			rs.close();

			return list;

		} catch (Exception e) {
			e.printStackTrace();
		}

		return list;
	}

	public static Product addProduct(Product product) {
		String sql = "Insert into HangHoa values(?,?,?,?)";

		try {

			PreparedStatement st = db.getConnection().prepareStatement(sql);
			st.setString(1, product.getCode());
			st.setString(2, product.getName());
			st.setFloat(3, product.getPrice());
			st.setString(4, product.getDes());

			int result = st.executeUpdate();
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return product;

	}

	public static Product deleteProduct() {
		Product product = null;
		String sql = "DELETE FROM HangHoa WHERE code=?";
		try {
			PreparedStatement ps = db.getConnection().prepareStatement(sql);
			ps.setString(1, product.getCode());

			int result = ps.executeUpdate();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return product;
	}

	public static  Product updateProduct(Product product) {
		String sql = "Update HangHoa set name=?,price=?,des=? where code=? ";

		try {
			PreparedStatement ps = db.getConnection().prepareStatement(sql);
			ps.setString(1, product.getName());
			ps.setFloat(2, product.getPrice());
			ps.setString(3, product.getDes());
			ps.setString(4, product.getCode());

			int result = ps.executeUpdate();
			

		} catch (Exception e) {
			e.printStackTrace();
		}
		return product;

		
	}
}
