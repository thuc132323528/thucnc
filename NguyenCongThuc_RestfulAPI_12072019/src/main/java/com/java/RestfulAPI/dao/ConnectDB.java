package com.java.RestfulAPI.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConnectDB {
	private static String db_url = "jdbc:sqlserver://localhost:50343;"
            + "databaseName=Sanpham;"
            + "integratedSecurity=true";
    private static String user = "sa";
    private static String pass = "123456";
 
    public static Connection getConnection() {
        Connection conn = null;
        try {
        	Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
            conn = DriverManager.getConnection(db_url, user, pass);
            System.out.println("connect successfully!");
        } catch (Exception ex) {
            System.out.println("connect failure!");
            ex.printStackTrace();
        }
        return conn;
    }
}
