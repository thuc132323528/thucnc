package com.java.RestfulAPI.model;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name="HangHoa")
@XmlAccessorType(XmlAccessType.FIELD)
public class Product {

	private String code;
	private String name;
	private float price;
	private String des;
	
	
	public Product() {
	}


	public Product(String code, String name, float price, String des) {
		this.code = code;
		this.name = name;
		this.price = price;
		this.des = des;
	}


	public String getCode() {
		return code;
	}


	public void setCode(String code) {
		this.code = code;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public float getPrice() {
		return price;
	}


	public void setPrice(float price) {
		this.price = price;
	}


	public String getDes() {
		return des;
	}


	public void setDes(String des) {
		this.des = des;
	}
	
	
}
	