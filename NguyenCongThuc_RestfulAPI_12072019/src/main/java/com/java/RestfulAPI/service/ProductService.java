package com.java.RestfulAPI.service;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.java.RestfulAPI.model.Product;
import com.java.RestfulAPI.dao.ProductDAO;



@Path("/products")
public class ProductService{
	@GET
	@Path("/getAll")
    @Produces({ MediaType.APPLICATION_JSON })
	public List<Product> showJson(){
		List<Product> list=ProductDAO.getproducts();
		return list;
	}
	
	@POST
	@Path("/add")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Product addProduct(Product pr) {
        return ProductDAO.addProduct(pr);
    }
	
	@PUT
	@Path("/update")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public Product updateEmployee(Product pr) {
        return ProductDAO.updateProduct(pr);
    }
	
	@DELETE
    @Path("/{delete}")
    @Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
    public void deleteEmployee(@PathParam("code") String code) {
        ProductDAO.deleteProduct();
    }
}
