package com.SpringBootRestful.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.SpringBootRestful.entity.Employee;
import com.SpringBootRestful.repository.EmployeeRepository;


@Controller
public class EmployeeController {
  @Autowired
  private EmployeeRepository e;
  @RequestMapping(value={"/", "/Employee-list"})
  public String listEmployee(Model model) {
    model.addAttribute("listEmployee", e.findAll());
    return "Employee-list";
  }
  @RequestMapping("/Employee-save")
  public String insertEmployee(Model model) {
    model.addAttribute("Employee", new Employee());
    return "Employee-save";
  }
  @RequestMapping("/Employee-view/{id}")
  public String viewEmployee(@PathVariable int id, Model model) {
    Optional<Employee> Employee = e.findById(id);
    if (Employee.isPresent()) {
      model.addAttribute("Employee", Employee.get());
    }
    return "Employee-view";
  }
  
  @RequestMapping("/Employee-update/{id}")
  public String updateEmployee(@PathVariable int id, Model model) {
    Optional<Employee> Employee = e.findById(id);
    if (Employee.isPresent()) {
      model.addAttribute("Employee", Employee.get());
    }
    return "Employee-update";
  }
  @RequestMapping("/saveEmployee")
  public String doSaveEmployee(@ModelAttribute("Employee") Employee Employee, Model model) {
    e.save(Employee);
    model.addAttribute("listEmployee", e.findAll());
    return "Employee-list";
  }
  @RequestMapping("/updateEmployee")
  public String doUpdateEmployee(@ModelAttribute("Employee") Employee Employee, Model model) {
    e.save(Employee);
    model.addAttribute("listEmployee", e.findAll());
    return "Employee-list";
  }
  
  @RequestMapping("/EmployeeDelete/{id}")
  public String doDeleteEmployee(@PathVariable int id, Model model) {
    e.deleteById(id);
    model.addAttribute("listEmployee", e.findAll());
    return "Employee-list";
  }
}
